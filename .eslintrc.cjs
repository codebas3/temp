module.exports = {
  env: {
    browser: true,
    es2021: true,
    'jest/globals': true
  },
  extends: 'standard',
  plugins: ['jest', 'eslint-plugin-html', 'eslint-plugin-json'],
  overrides: [
    {
      env: {
        node: true
      },
      files: [
        '.eslintrc.{js,cjs}'
      ],
      parserOptions: {
        sourceType: 'script'
      }
    }
  ],
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module'
  },
  rules: {
    'jest/no-disabled-tests': 'warn',
    'jest/no-focused-tests': 'error',
    'jest/no-identical-title': 'error',
    'jest/prefer-to-have-length': 'warn',
    'jest/valid-expect': 'error'
  },
  ignorePatterns: [
    'Dockerfile',
    'node_modules',
    'dist',
    'src/page/asset',
    'src/page/css',
    'src/page/lib',
    '*.proto',
    '*.ejs',
    '*.md',
    '*.sh'
  ],
  settings: {
    'import/resolver': {
      node: {
        moduleDirectory: ['node_modules'],
        paths: ['.']
      }
    }
  }
}
