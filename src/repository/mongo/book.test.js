import { MongoMemoryServer } from 'mongodb-memory-server'
import Redis from '../../adapter/redis'
import MongoDB from '../../adapter/mongodb'
import { fetchBooks, fetchIncrement, createBook } from './book.js'

const MongodServer = await MongoMemoryServer.create()
const mongodb = await MongoDB.getInstance({ uri: MongodServer.getUri() })
const { mongoose } = mongodb
const { ioredis } = await Redis.getInstance()

afterAll(async () => {
  await mongodb.disconnect()
  await MongodServer.stop()
})

describe('Books service', () => {
  describe('getNextId', () => {
    it('should return the next of increment', async () => {
      const results = await MongoDB.getNextId('account')
      expect(results).toBeGreaterThan(0)
    })
  })

  describe('fetchIncrement', () => {
    it('should return the list of increment', async () => {
      const results = await fetchIncrement()
      expect(results[0].val).toBeGreaterThan(0)
    })
  })

  describe('createBook', () => {
    it('should return new Book', async () => {
      const result = await createBook({
        title: 'Book 1',
        author: new mongoose.Types.ObjectId(),
        year: 2001
      })
      expect(result.year).toBe(2001)
    })
  })

  describe('fetchBooks', () => {
    it('should return the list of books', async () => {
      const results = await fetchBooks()
      expect(results[0].title).toBe('Book 1')
    })
  })

  describe('set and get key from redis', () => {
    it('should return the list of books', async () => {
      const setter = await ioredis.set('a', 13)
      expect(setter).toBe('OK')

      const getter = await ioredis.get('a')
      expect(getter).toBe('13')
    })
  })
})
