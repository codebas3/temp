import MongoDB from '../../adapter/mongodb.js'

const fetchBooks = async () => {
  const { mongoose } = await MongoDB.getInstance()
  return mongoose.models.Book.find({}).exec()
}

const fetchBook = async id => {
  const { mongoose } = await MongoDB.getInstance()
  return mongoose.models.Book.findById(id).exec()
}

const createBook = async ({ title, author, year }) => {
  const { mongoose } = await MongoDB.getInstance()
  const book = new mongoose.models.Book({
    title,
    author,
    year
  })
  return book.save()
}

const fetchIncrement = async () => {
  const { mongoose } = await MongoDB.getInstance()
  return mongoose.models.Increment.find({}).exec()
}

export {
  fetchBooks,
  fetchBook,
  createBook,
  fetchIncrement
}
