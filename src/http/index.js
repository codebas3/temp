import Fastify from 'fastify'
import * as Handler from './api/v1/handler/root.js'

export default class Http {
  app = Fastify({
    logger: true
  })

  constructor () {
    this.app.get('/', Handler.getRoot)
  }

  async start () {
    try {
      await this.app.listen({ port: 3000 })
    } catch (err) {
      this.app.log.error(err)
      process.exit(1)
    }
  }
}
