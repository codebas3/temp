import * as BookService from '../../../../service/book.js'

export const getRoot = async function (request, reply) {
  const result = await BookService.welcome()
  reply.send(result)
}
