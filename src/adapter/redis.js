import { existsSync, readFileSync } from 'fs'
import IORedis from 'ioredis'
import IORedisMock from 'ioredis-mock'

const factoryConfig = {
  host: '127.0.0.1',
  port: '6379',
  family: 4,
  db: '0',
  username: '',
  password: '96fdQPBFxp2ADueHhMSpuDy3'
}

export default class Redis {
  constructor (config = factoryConfig) {
    this.config = config
  }

  static async getInstance (config) {
    if (!this.instance) {
      const i = this.instance = new Redis(config)
      i.ioredis = await i.connect()
    }

    return this.instance
  }

  async connect () {
    const { ssl, host, port, username, password: passwd, db } = this.config
    const protocol = 'redis' + (ssl ? 's' : '')

    const password = existsSync(passwd) ? readFileSync(passwd).toString().replace(/\n$/, '') : passwd
    const acl = password ? `${username}:${password}@` : ''
    const uri = `${protocol}://${acl}${host}:${port}`

    const isTest = process.env.NODE_ENV === 'test'
    const ioredis = new (isTest ? IORedisMock : IORedis)(uri)
    if (!isTest) {
      ioredis.on('connect', () => console.debug('redis connect'))
      ioredis.on('ready', () => console.debug('redis ready'))
      ioredis.on('error', error => console.error('redis error:', error.message))
      ioredis.on('close', () => console.info('redis closed'))
      ioredis.on('reconnecting', () => console.debug('redis reconnecting'))
      ioredis.on('end', () => console.info('redis end'))
      ioredis.on('wait', () => console.debug('redis waiting'))
      ioredis.on('select', (db) => console.debug('redis select db:', db))
      if (db) await ioredis.select(db)
    }
    return ioredis
  }

  async disconnect () {
    return this.ioredis.disconnect()
  }
}
