import { mongoose, Schema } from 'mongoose'
import * as Model from '../model/mongo/index.js'

const factoryConfig = {
  ssl: false,
  protocol: 'mongodb',
  host: 'localhost',
  port: '27017',
  user: 'root',
  pass: '96fdQPBFxp2ADueHhMSpuDy3',
  dbName: 'newer',
  pool: {
    bufferCommands: true,
    minPoolSize: 0,
    maxPoolSize: 5,
    connectTimeoutMS: 30000,
    socketTimeoutMS: 30000,
    serverSelectionTimeoutMS: 5000,
    heartbeatFrequencyMS: 1000
  },
  family: 4,
  useCreateIndex: true,
  autoIndex: true,
  debugConnection: true
}

export default class MongoDB {
  constructor (config = factoryConfig) {
    this.mongoose = mongoose
    this.config = config
  }

  static async getInstance (config) {
    if (!this.instance) {
      const i = this.instance = new MongoDB(config)
      i.mongoose = await i.connect()
      i.models = await i.initModel(i.mongoose)
    }

    return this.instance
  }

  static async getNextId (context) {
    const model = this.instance.models.Increment
    const condition = { _id: context }

    const { val } = await model.findOneAndUpdate(
      condition,
      { _id: context, $inc: { val: 1 }, createdBy: 'SYSTEM' },
      { upsert: true, new: true, lean: true }
    ).exec()

    return val
  }

  async initModel (mongoose) {
    mongoose.model(
      Model.Increment.model,
      new Schema(Model.Increment.schema, { collection: Model.Increment.collection })
    )
    mongoose.model(
      Model.Book.model,
      new Schema(Model.Book.schema, { collection: Model.Book.collection })
    )

    return mongoose.models
  }

  async connect () {
    const { ssl, protocol, host, port: p, user, pass, dbName, family, autoIndex, pool } = this.config

    try {
      let { uri } = this.config
      if (!uri) {
        let port = `:${p}`
        if (protocol.indexOf('srv')) port = ''

        let qs = '?'
        if (ssl) qs += '&ssl=true'

        uri = `${protocol}://${host}${port}/${qs}`
      }

      if ([undefined, true].indexOf(this.config?.debugConnection) > -1) {
        const conn = mongoose.connection
        conn.on('connecting', (...args) => console.debug('mongoose connecting'))
        conn.on('connected', (...args) => console.debug('mongoose connected'))
        conn.on('disconnecting', (...args) => console.debug('mongoose disconnecting'))
        conn.on('disconnected', (...args) => console.info('mongoose disconnected'))
        conn.on('close', (...args) => console.info('mongoose close'))
        conn.on('reconnected', (...args) => console.debug('mongoose reconnected'))
        conn.on('reconnectFailed', (...args) => console.error('mongoose reconnect failed'))
        conn.on('error', (...args) => console.error('mongoose error'))
        conn.on('fullsetup', (...args) => console.debug('mongoose fully setup'))
        conn.on('all', (...args) => console.debug('mongoose all connected'))
      }

      const option = { user, pass, dbName, family, autoIndex, ...pool }
      const client = await mongoose.connect(uri, option) // it's return mongoose too :)

      return client
    } catch (err) {
      console.error('something wrong', err.message)

      if (err.parent) {
        if (err.parent.message !== err.message) {
          console.error('mongodb error', err.parent)
        }
      }

      if (err.original) {
        if (err.parent) {
          if (err.original.message !== err.parent.message) {
            console.error('mongodb error', err.original)
          }
        }
      }

      await this.disconnect()
    }
  }

  async disconnect () {
    return this.mongoose.connection.close()
  }
}
