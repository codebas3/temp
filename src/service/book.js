import * as BookRepository from '../repository/mongo/book.js'

export const welcome = async function () {
  return {
    message: 'Eyyo!',
    data: await BookRepository.fetchIncrement()
  }
}
