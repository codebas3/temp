const IncrementSchema = {
  _id: String,
  val: Number
}

const Increment = {
  model: 'Increment',
  collection: 'increment',
  schema: IncrementSchema
}

export default Increment
