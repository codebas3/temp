import { Schema } from 'mongoose'

const BookSchema = {
  title: {
    type: String,
    trim: true
  },
  author: {
    type: Schema.Types.ObjectId,
    ref: 'authors'
  },
  year: {
    type: Number
  }
}

const Book = {
  model: 'Book',
  collection: 'book',
  schema: BookSchema
}

export default Book
